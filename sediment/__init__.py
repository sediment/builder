import contextlib
import tarfile

from .buildah import Container, Image

def debootstrap(*args) -> Image:
    """
    Runs debootstrap and produces an Image
    """
    with Container('scratch') as dest, \
         dest.mount() as root, \
         Container(Image.pull('debian'), mounts=[(root, '/dest')]) as workspace:
        workspace.run(['apt-get', 'update'])
        # XXX: debootstrap or cdebootstrap?
        workspace.run(['apt-get', 'install', '-y' 'debootstrap'])

        if '://' in args[-1]:
            # A repository URL was given, which goes after the destination
            cmd = args[:-1] + ['/dest'] + args[-1:]
        else:
            cmd = args + ['/dest']

        workspace.run(['debootstrap', *cmd])

        return dest.commit()


def import_tarball(source) -> Image:
    """
    Produces an Image from a given tarball
    """
    if isinstance(source, str) or hasattr(source, '__fspath__'):
        # Given a path to open
        tar = tarfile.open(source, 'r|*')
    else:
        # Assume a file-like object
        tar = tarfile.open(fileobj=source, mode='r|*')
    with tar, Container('scratch') as dest, dest.mount() as root:
        tar.extractall(root)
        return dest.commit()
