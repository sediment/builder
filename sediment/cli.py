import os
import sys

import click


@click.group()
def main():
    pass


@main.command('import')
def import_():
    """
    Import a disk into sediment.
    """
    click.echo('TODO')


@main.command()
def export():
    """
    Export from sediment into a disk
    """
    click.echo('TODO')


@main.command()
def build():
    """
    Build an image from a file
    """
    click.echo('TODO')


def wrapper():
    if '_CONTAINERS_USERNS_CONFIGURED' in os.environ:
        # We're in the buildah unshare environment
        return main()
    else:
        # We're not in the unshare environment, trampoline
        os.execvp('buildah', ['buildah', 'unshare', *sys.argv])
